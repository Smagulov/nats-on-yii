<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Nats\ConnectionOptions;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {
        $options = new ConnectionOptions([
            'host' => 'c14b28a9dc5b'
        ]);

        $client = new \Nats\Connection($options);
        $client->connect();


        $client->queueSubscribe(
            'foo',
            'test',
            function ($message) {
                printf("Data: %s\r\n", $message->getBody());
            }
        );

        $client->wait(0);

        return ExitCode::OK;
    }
}
